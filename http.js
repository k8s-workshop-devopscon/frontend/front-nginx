function version(r) {
    r.headersOut['Content-Type'] = "application/json; charset=utf-8";
    r.return(200, '{"APP_VERSION":' + process.env.APP_VERSION + '}');
}

export default {version};

